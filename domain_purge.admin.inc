<?php

/**
 * @file
 * Contains code for actually doing the administrative task of purging a domain.
 */

/**
 * Show the form to delete the domain. Menu callback.
 */
function domain_purge_form($form, $form_state, $domain) {
  $form = array(
    'domain' => array(
      '#type' => 'value',
      '#value' => $domain,
    ),
    'content_opt' => array(
      '#type' => 'radios',
      '#title' => t('Delete content assigned to this domain'),
      '#options' => array(
        'none' => t('Don&rsquo;t delete any content'),
        'only' => t('Delete content assigned only to this domain and no others'),
        'all' => t('Delete all content assigned to this domain (does not include content marked &ldquo;Assign to all affiliates&rdquo;)'),
      ),
      '#default_value' => 'only',
    ),
    'user_opt' => array(
      '#type' => 'radios',
      '#title' => t('Delete users assigned to this domain'),
      '#options' => array(
        'none' => t('Don&rsquo;t delete any users'),
        'only' => t('Delete users assigned only to this domain and no others'),
        'all' => t('Delete all users assigned to this domain'),
      ),
      '#default_value' => 'only',
    ),
  );
  return confirm_form(
    $form,
    // The <strong>s below strike me as odd, but it's what DA itself does.
    t('Are you sure you wish to delete the domain record and assigned content and users for <strong>%domain</strong>?', array('%domain' => $domain['subdomain'])),
    'admin/structure/domain/view',
    NULL,
    t('Delete domain record and assigned content and users'),
    t('Cancel')
  );
}

/**
 * Submission function for the domain purge confirmation form.
 */
function domain_purge_form_submit($form, $form_state) {
  $batch = array(
    'title' => t('Purging domain'),
    'operations' => array(),
    'finished' => 'domain_purge_batch_finished',
    'file' => drupal_get_path('module', 'domain_purge') . '/domain_purge.admin.inc',
  );
  if ($form_state['values']['content_opt'] !== 'none') {
    $nids_to_analyze = db_select('domain_access', 'da')
      ->fields('da', array('nid'))
      ->condition('realm', 'domain_id')
      ->condition('gid', $form_state['values']['domain']['domain_id'])
      ->execute()
      ->fetchCol();
    if ($nids_to_analyze) {
      $batch['operations'][] = array('domain_purge_batch_nodes', array($nids_to_analyze, $form_state['values']['content_opt'] == 'all'));
    }
  }
  if ($form_state['values']['user_opt'] !== 'none') {
    $uids_to_analyze = db_select('domain_editor', 'de')
      ->fields('de', array('uid'))
      ->condition('domain_id', $form_state['values']['domain']['domain_id'])
      ->execute()
      ->fetchCol();
    if ($uids_to_analyze) {
      $batch['operations'][] = array('domain_purge_batch_users', array($uids_to_analyze, $form_state['values']['user_opt'] == 'all'));
    }
  }
  $batch['operations'][] = array('domain_purge_batch_domain', array($form_state['values']['domain']['domain_id']));
  batch_set($batch);
  batch_process('admin/structure/domain');
}

/**
 * Delete domain nodes. Batch callback.
 */
function domain_purge_batch_nodes($nids_to_analyze, $del_all, $context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['index'] = 0;
    $context['sandbox']['count'] = count($nids_to_analyze);
    $context['sandbox']['deleted'] = 0;
  }
  $node = node_load($nids_to_analyze[$context['sandbox']['index']]);
  if ($node) {
    // If this node is not published to all domains and either we're deleting
    // all nodes or this node is only assigned to one site (which will be this
    // one, unless it was assigned to another site while the batch process was
    // running)…
    if (empty($node->domain_site) && ($del_all || count($node->domains) === 1)) {
      node_delete($node->nid);
      $context['sandbox']['deleted']++;
    }
  }
  $context['sandbox']['index']++;
  $context['finished'] = $context['sandbox']['index'] / $context['sandbox']['count'];
  if ($context['finished'] === 1) {
    $context['results'][] = format_plural(
      $context['sandbox']['deleted'],
      '1 node deleted.',
      '@count nodes deleted.'
    );
  }
  $context['message'] = format_plural(
    $context['sandbox']['count'],
    '1 of 1 node processed.',
    '@index of @count nodes processed.',
    array('@index' => $context['sandbox']['index'])
  );
}

/**
 * Delete domain users. Batch callback.
 */
function domain_purge_batch_users($uids_to_analyze, $del_all, $context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['index'] = 0;
    $context['sandbox']['count'] = count($uids_to_analyze);
    $context['sandbox']['deleted'] = 0;
  }
  $account = user_load($uids_to_analyze[$context['sandbox']['index']]);
  if ($account && $account->uid != 1) {
    if ($del_all || count($account->domain_user) === 1) {
      user_delete($account->uid);
      $context['sandbox']['deleted']++;
    }
  }
  $context['sandbox']['index']++;
  $context['finished'] = $context['sandbox']['index'] / $context['sandbox']['count'];
  if ($context['finished'] === 1) {
    $context['results'][] = format_plural(
      $context['sandbox']['deleted'],
      '1 user deleted.',
      '@count users deleted.'
    );
  }
  $context['message'] = format_plural(
    $context['sandbox']['count'],
    '1 of 1 user processed.',
    '@index of @count users processed.',
    array('@index' => $context['sandbox']['index'])
  );
}

/**
 * Delete a domain. Batch callback.
 */
function domain_purge_batch_domain($domain_id, $context) {
  domain_delete(domain_load($domain_id));
  $context['finished'] = 1;
  $context['message'] = t('Deleting domain record.');
}

/**
 * Batch finished callback. Tell the users what happened.
 */
function domain_purge_batch_finished($success, $results, $operations) {
  if ($success) {
    foreach ($results as $result) {
      drupal_set_message($result);
    }
    drupal_set_message(t('The domain has been purged.'));
  }
}
